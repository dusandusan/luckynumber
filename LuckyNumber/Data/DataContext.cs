﻿using LuckyNumber.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LuckyNumber.Data
{
    public class DataContext : IDataContext
    {
        private Dictionary<Guid, Round> _Rounds;

        private Dictionary<Guid, Ticket> _Tickets;

        private bool _FreezeStatus;

        internal DataContext()
        {
            this._Rounds = new Dictionary<Guid, Round>();
            this._Tickets = new Dictionary<Guid, Ticket>();
            this._FreezeStatus = false;
        }

        public void AddEntity<TEntity>(BaseEntity entity) where TEntity : BaseEntity
        {
            switch (typeof(TEntity).Name)
            {
                case "Round":
                    Round r = (Round)entity;
                    _Rounds.Add(r.Id, r);
                    break;
                case "Ticket":
                    Ticket t = (Ticket)entity;
                    _Tickets.Add(t.Id, t);
                    break;
            }
        }

        public void UpdateEntity<TEntity>(BaseEntity entity) where TEntity : BaseEntity
        {
            switch (typeof(TEntity).Name)
            {
                case "Round":
                    Round r = (Round)entity;
                    _Rounds[r.Id] = r;
                    break;
                case "Ticket":
                    Ticket t = (Ticket)entity;
                    _Tickets[t.Id] = t;
                    break;
            }
        }

        public TEntity GetEntity<TEntity>(Guid id) where TEntity : BaseEntity
        {
            BaseEntity entity = new BaseEntity();

            switch (typeof(TEntity).Name)
            {
                case "Round":
                    entity = _Rounds.FirstOrDefault(x => x.Key == id).Value;
                    break;
                case "Ticket":
                    entity = _Tickets.FirstOrDefault(x => x.Key == id).Value;
                    break;
            }

            return (TEntity)entity;
        }

        public Round ActiveRound()
        {
            return _Rounds.FirstOrDefault(x => x.Value.State == RoundState.ACTIVE).Value;
        }

        public List<Ticket> TicketsForRound(Guid id)
        {
            return _Tickets.Where(x => x.Value.RoundId == id).Select(x => x.Value).ToList();
        }


        public bool GetFreezeStatus()
        {
            return _FreezeStatus;
        }

        public void UpdateFreezeStatus(bool status)
        {
            _FreezeStatus = status;
        }

    }
}
