﻿using System;
using System.Collections.Generic;
using LuckyNumber.Model;

namespace LuckyNumber.Data
{
    public interface IDataContext
    {
        void AddEntity<TEntity>(BaseEntity entity) where TEntity : BaseEntity;

        void UpdateEntity<TEntity>(BaseEntity entity) where TEntity : BaseEntity;

        TEntity GetEntity<TEntity>(Guid id) where TEntity : BaseEntity;

        Round ActiveRound();

        bool GetFreezeStatus();

        void UpdateFreezeStatus(bool status);
    }
}