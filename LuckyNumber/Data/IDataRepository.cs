﻿using System;
using System.Collections.Generic;
using LuckyNumber.Model;

namespace LuckyNumber.Data
{
    public interface IDataRepository
    {
        void Add<TEntity>(BaseEntity entity) where TEntity : BaseEntity;

        TEntity Get<TEntity>(Guid id) where TEntity : BaseEntity;

        void Update<TEntity>(TEntity entity) where TEntity : BaseEntity;

        Round ActiveRound();

        bool GetFreezeStatus();

        void UpdateFreezeStatus(bool status);

        List<Ticket> TicketsForRound(Guid id);
    }
}