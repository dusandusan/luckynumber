﻿using LuckyNumber.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LuckyNumber.Data
{
    public class DataRepository : IDataRepository
    {
        private readonly DataContext _ctx;

        public DataRepository()
        {
            this._ctx = new DataContext();
        }

        public void Add<TEntity>(BaseEntity entity) where TEntity : BaseEntity
        {
            _ctx.AddEntity<TEntity>(entity);
        }

        public TEntity Get<TEntity>(Guid id) where TEntity : BaseEntity
        {
            return _ctx.GetEntity<TEntity>(id);
        }

        public Round ActiveRound()
        {
            return _ctx.ActiveRound();
        }

        public void Update<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            _ctx.UpdateEntity<TEntity>(entity);
        }

        public bool GetFreezeStatus()
        {
            return _ctx.GetFreezeStatus();
        }

        public void UpdateFreezeStatus(bool status)
        {
            _ctx.UpdateFreezeStatus(status);
        }

        public List<Ticket> TicketsForRound(Guid id)
        {
            return _ctx.TicketsForRound(id);
        }
    }
}
