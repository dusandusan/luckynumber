﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LuckyNumber.Model
{
    public class Ticket : BaseEntity
    {
        public int Payin { get; set; }

        public List<int> Numbers { get; set; }

        public PorcessedStatus ProcessedStatus { get; set; }

        public WinnerStatus WinnerStatus { get; set; }

        public double Quota { get; private set; }

        public double Payout { get; private set; }

        public Guid RoundId { get; set; }

        public Ticket(Guid roundId, List<int> numbers, int payin, double OperatorShare, double MaxTicketPayout) : base()
        {
            this.RoundId = roundId;
            this.Numbers = numbers;
            this.Payin = payin;
            this.ProcessedStatus = PorcessedStatus.NOT_PROCESSED;
            this.WinnerStatus = WinnerStatus.NONE;
            this.Quota = 10 / Numbers.Count() * (100 - OperatorShare) / 100;
            double payout = Quota * Payin;
            this.Payout = payout > MaxTicketPayout ? MaxTicketPayout : payout;
        }

    }
}
