﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LuckyNumber.Model
{

    public enum RoundState
    {
        ACTIVE,
        COMPLETED
    }

    public enum PorcessedStatus
    {
        NOT_PROCESSED,
        PROCESSED
    }

    public enum WinnerStatus
    {
        NONE,
        WON,
        LOST
    }

    public class PayTicketRequest
    {
        public int Payin { get; set; }
        public List<int> Numbers { get; set; }
    }

    public class CreateRoundRequest
    {
        public int Duration { get; set; }
        public double MaxRoundPayout { get; set; }
        public double MaxTicketPayout { get; set; }
    }

    public class ProcessedRound
    {
        public int WinnerNumber { get; set; }
        public List<Ticket> WinnerTickets { get; set; }
        public List<Ticket> LooserTickets { get; set; }

        public ProcessedRound()
        {
            WinnerTickets = new List<Ticket>();
            LooserTickets = new List<Ticket>();
        }
    }

}
