﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LuckyNumber.Model
{
    public class Round : BaseEntity
    {
        public DateTime EndTime { get; set; }

        public double MaxRoundPayout { get; set; }

        public double MaxTicketPayout { get; set; }

        public RoundState State { get; set; }

        public Dictionary<int, List<Ticket>> PlayedNumbers { get; set; }

        public int WinnerNumber { get; set; }

        public Round() : base()
        {
            this.State = RoundState.ACTIVE;
            this.PlayedNumbers = new Dictionary<int, List<Ticket>>();

            for (int i = 1; i <= 10; i++)
            {
                List<Ticket> l = new List<Ticket>();
                this.PlayedNumbers.Add(i, l);
            }
        }

        public bool AddTicket(Ticket ticket)
        {
            if (MaxPayoutPerNumber(ticket) > MaxRoundPayout)
            {
                return false;
            }
            foreach (int num in ticket.Numbers)
            {
                switch (num)
                {
                    case 1:
                        PlayedNumbers[1].Add(ticket);
                        break;
                    case 2:
                        PlayedNumbers[2].Add(ticket);
                        break;
                    case 3:
                        PlayedNumbers[3].Add(ticket);
                        break;
                    case 4:
                        PlayedNumbers[4].Add(ticket);
                        break;
                    case 5:
                        PlayedNumbers[5].Add(ticket);
                        break;
                    case 6:
                        PlayedNumbers[6].Add(ticket);
                        break;
                    case 7:
                        PlayedNumbers[7].Add(ticket);
                        break;
                    case 8:
                        PlayedNumbers[8].Add(ticket);
                        break;
                    case 9:
                        PlayedNumbers[9].Add(ticket);
                        break;
                    case 10:
                        PlayedNumbers[10].Add(ticket);
                        break;
                }
            }
            return true;
        }

        public double MaxPayoutPerNumber(Ticket t)
        {
            return PlayedNumbers.Max(number => (number.Value.Sum(ticket => (ticket.Payout)) + (t.Numbers.Contains(number.Key) ? t.Payout : 0)));
        }

        public KeyValuePair<int, List<Ticket>> GetNumberWithMinPayout()
        {
            List<int> zeroSum = new List<int>();
            bool first = true;
            double minSum = 0;
            KeyValuePair<int, List<Ticket>> winner = new KeyValuePair<int, List<Ticket>>();
            foreach (KeyValuePair<int, List<Ticket>> item in PlayedNumbers)
            {
                double payout = item.Value.Sum(x => x.Payout);
                if (payout == 0)
                {
                    zeroSum.Add(item.Key);
                }
                if (first)
                {
                    minSum = payout;
                    winner = item;
                    first = false;
                }
                else
                {
                    if (payout < minSum)
                    {
                        minSum = payout;
                        winner = item;
                    }
                }
            }
            if (zeroSum.Count > 1)
            {
                Random rnd = new Random();
                int randomNum = rnd.Next(zeroSum.Count);
                return PlayedNumbers.First(x => x.Key == zeroSum[randomNum]);
            }
            else
            {
                return winner;
            }

        }

        public ProcessedRound GetWinnerList()
        {
            KeyValuePair<int, List<Ticket>> winner = this.GetNumberWithMinPayout();

            ProcessedRound procRound = new ProcessedRound();

            procRound.WinnerNumber = winner.Key;
            procRound.WinnerTickets = winner.Value;

            foreach (var item in PlayedNumbers)
            {
                if (item.Key == procRound.WinnerNumber)
                    continue;
                else
                {
                    procRound.LooserTickets.AddRange(item.Value.Distinct());
                }
            }
            return procRound;
        }
    }

}
