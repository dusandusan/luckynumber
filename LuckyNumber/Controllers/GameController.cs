﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LuckyNumber.Data;
using LuckyNumber.Model;
using System.Threading;
using Microsoft.Extensions.Options;

namespace LuckyNumber.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class GameController : Controller
    {
        private readonly IDataRepository _repository;
        private readonly GameSettings _settings;

        public GameController(IDataRepository repository, IOptions<GameSettings> settings)
        {
            this._repository = repository;
            this._settings = settings.Value;
        }

        #region Action methods
        /* 01. Kreiraj novu rundu
        Primer json modela iz tela poziva:
        {
	        "Duration":"360",
	        "MaxRoundPayout":"100",
	        "MaxTicketPayout": 50
        }
        */
        [HttpPost]
        public IActionResult CreateRound([FromBody]CreateRoundRequest model)
        {
            try
            {
                Round r = _repository.ActiveRound();

                if (r != null)
                {
                    return Ok(String.Format("Ther is active rounde at the moment, you can create new round after {0}", r.EndTime));
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                Round round = new Round();

                int duration = (model.Duration - (_settings.FreezeTime == 0 ? 5 : _settings.FreezeTime)) * 1000;
                DateTime endTime = round.CreatedDate.AddSeconds(model.Duration);

                Timer t = new Timer(new TimerCallback(EndRound));
                t.Change(duration, 0);

                round.EndTime = endTime;
                round.MaxRoundPayout = model.MaxRoundPayout;
                round.MaxTicketPayout = model.MaxTicketPayout;

                _repository.Add<Round>(round);

                return Ok(round.Id);

            }
            catch (Exception e)
            {

                return BadRequest("Server error" + e);
            }
        }

        /* 02. Uplati tiket
        Primer json modela iz tela poziva:
        {
	        "Payin":"100",
	        "Numbers":[9,2,5,8]
        }
        */
        [HttpPost]
        public IActionResult PayTicket([FromBody]PayTicketRequest model)
        {
            Round r = _repository.ActiveRound();

            if (_repository.GetFreezeStatus())
            {
                return Ok("No more bet in this round!");
            }

            if (r == null)
            {
                return Ok("There is no active round at the moment, please try later");
            }
            int minPayin = _settings.MinPayin == 0 ? 20 : _settings.MinPayin;

            if (model.Payin < minPayin)
            {
                return Ok(String.Format(String.Format("Minimal payin is {0}", minPayin)));
            }

            bool outOfRange = false;
            bool sameNumbers = false;

            ValidateNumbers(model, ref outOfRange, ref sameNumbers);

            if (outOfRange || sameNumbers)
            {
                return Ok("Your choosen numbers are out of range(1 - 10) or duplicated");
            }

            Ticket ticket = new Ticket(r.Id, model.Numbers, model.Payin, _settings.OperatorShare, r.MaxTicketPayout);

            bool ticketAdded = r.AddTicket(ticket);

            if (ticketAdded)
            {
                _repository.Add<Ticket>(ticket);

                _repository.Update(r);

                return Ok(ticket.Id);
            }
            else
            {
                return Ok("Ticket refused! Maximal payout reached");
            }
        }

        /* 03. Vrati podatke o rundi
        Primer json modela iz tela poziva:
        {
           "id":"238f856e-37b9-4cdd-8c31-b6a78c194762"
         }
        */
        [HttpPost]
        public IActionResult RoundStatus(Guid id)
        {
            Round r = _repository.Get<Round>(id);

            if (r == null)
            {
                Ok("Round with given id does not exist");
            }
            List<Ticket> tickets = _repository.TicketsForRound(id);

            double totalPayout = 0;

            if (r.State == RoundState.ACTIVE)
            {
                totalPayout = tickets.Sum(x => x.Payout);
            }
            else
            {
                totalPayout = tickets.Where(x => x.WinnerStatus == WinnerStatus.WON).Sum(x => x.Payout);
            }


            return Json(new { id = r.Id, status = r.State.ToString(), start = r.CreatedDate.ToString(), end = r.EndTime.ToString(), total_tickets = tickets.Count(), total_payin = tickets.Sum(x => x.Payin), total_payout = totalPayout });
        }
        /* 04. Vrati podatke o tiketu
        Primer json modela iz tela poziva:
        {
           "id":"238f856e-37b9-4cdd-8c31-b6a78c194762"
        }
        */
        [HttpPost]
        public IActionResult TicketStatus(Guid id)
        {
            Ticket t = _repository.Get<Ticket>(id);

            if (t == null)
            {
                Ok("Ticket with given id does not exist");
            }

            return Json(new { id = t.Id, payout = t.Payout, processed_status = t.ProcessedStatus.ToString(), won_status = t.WinnerStatus.ToString() });
        }
        #endregion

        #region Private Methods

        private static void ValidateNumbers(PayTicketRequest model, ref bool outOfRange, ref bool sameNumbers)
        {
            List<int> checkList = new List<int>();

            foreach (int num in model.Numbers)
            {
                if (num < 1 && num > 9)
                {
                    outOfRange = true;
                    break;
                }
                if (checkList.Count() > 1)
                {
                    if (checkList.Contains(num))
                    {
                        sameNumbers = true;
                        break;
                    }
                }
                checkList.Add(num);
            }
        }


        private void EndRound(object state)
        {
            if (_repository.GetFreezeStatus() == false)
            {
                _repository.UpdateFreezeStatus(true);

                Timer t = (Timer)state;
                t.Change(_settings.FreezeTime * 1000, 0);

                DecideWiner();
            }
            else
            {
                Round round = _repository.ActiveRound();
                round.State = RoundState.COMPLETED;
                _repository.Update(round);

                Timer t = (Timer)state;
                t.Dispose();
                _repository.UpdateFreezeStatus(false);
            }

        }

        private void DecideWiner()
        {
            Round r = _repository.ActiveRound();

            ProcessedRound pr = r.GetWinnerList();

            r.WinnerNumber = pr.WinnerNumber;
            _repository.Update(r);

            foreach (var ticket in pr.LooserTickets)
            {
                ticket.ProcessedStatus = PorcessedStatus.PROCESSED;
                ticket.WinnerStatus = WinnerStatus.LOST;
                _repository.Update(ticket);
            }
            foreach (var ticket in pr.WinnerTickets)
            {
                ticket.ProcessedStatus = PorcessedStatus.PROCESSED;
                ticket.WinnerStatus = WinnerStatus.WON;
                _repository.Update(ticket);
            }
        }
    }
    #endregion

}