﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LuckyNumber
{
    public class GameSettings
    {
        public int MinPayin { get; set; }
        public int FreezeTime { get; set; }
        public int OperatorShare { get; set; }
    }
}
